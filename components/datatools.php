<?php
/**
 * Datatools.php - Handy functions with database content checks
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';

/**
 * Returns TRUE if and only if the given userid already exists in the database.
 *
 * @param unknown $userid        	
 *
 * @return TRUE if and only if the given userid already exists in the database. Otherwise FALSE
 */
function useridExists($userid) {
	global $mysqli;
	$sql = "SELECT * FROM USER WHERE Userid='" . $userid . "';";
	$result = $mysqli->query ( $sql );
	if ($result->num_rows > 0) {
		return true;
	}
	return false;
}

/**
 * Returns TRUE if and only if the given email already exists in the USER table.
 *
 * @param unknown $userid        	
 *
 * @return TRUE if and only if the given email already exists in the USER table. Otherwise FALSE
 */
function emailExistsinUser($email) {
	global $mysqli;
	$sql = "SELECT * FROM USER WHERE Email='" . $email . "';";
	$result = $mysqli->query ( $sql );
	if ($result->num_rows > 0) {
		return true;
	}
	return false;
}

/**
 * Returns TRUE if and only if the given email already exists in the MESSAGE table
 *
 * @param unknown $email        	
 *
 * @return TRUE if and only if the given email already exists in the MESSAGE table. Otherwise FALSE
 */
function emailExistsInMessage($email) {
	global $mysqli;
	$sql = "SELECT * FROM MESSAGE WHERE Email='" . $email . "';";
	$result = $mysqli->query ( $sql );
	if ($result && $result->num_rows > 0) {
		return true;
	}
	return false;
}

/**
 * Returns a hash from the specified password
 * 
 * @param unknown $pwd
 *        	the password to be hashed
 */
function hashPassword($pwd) {
	$salt = "qweoiurfy48uypy84rf$^^^%WCqtwvjtv5jwltv q4Q4 %Q45q%q45Q45q5q5qW%5qw";
	return crypt ( $pwd, $salt );
}

